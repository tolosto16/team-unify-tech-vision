import React from 'react';
import './App.scss';
import Header from "./components/layout/Header";
import Landing from "./pages/home/Home";
import Sidebar from "./components/layout/Sidebar";

function App() {
    return (
        <div className="layout">
            <Header />
            <div className="layout-content">
                <Sidebar />
                <div className="main-content">
                    <Landing />
                </div>
            </div>
        </div>
    );
}

export default App;
