import React from 'react';
import './Sidebar.scss';
import { Menu } from 'antd';
import { Link } from 'react-router-dom';

import home from '../../assets/img/home_2.svg';
import users from '../../assets/img/user_2.svg';
import mailing from '../../assets/img/send_2.svg';
import analytics from '../../assets/img/lucide_bar-chart.svg';
import comments from '../../assets/img/Vector.svg';

const Sidebar: React.FC = () => {
    return (
        <div className="sidebar">
            <Menu
                defaultSelectedKeys={['1']}
                mode="vertical"
                className="sidebar-menu"
            >
                <Menu.Item key="1" icon={<img className="sidebar-icon" src={home} alt="Home" />}>
                    <Link to="/">Головна</Link>
                </Menu.Item>
                <Menu.Item key="2" icon={<img className="sidebar-icon" src={users} alt="Users" />}>
                    <Link to="/users">Користувачі</Link>
                </Menu.Item>
                <Menu.Item key="3" icon={<img className="sidebar-icon" src={mailing} alt="Mailing" />}>
                    <Link to="/mailing">Розсилка</Link>
                </Menu.Item>
                <Menu.Item key="4" icon={<img className="sidebar-icon" src={analytics} alt="Analytics" />}>
                    <Link to="/analytics">Аналітика</Link>
                </Menu.Item>
                <Menu.Item key="5" icon={<img className="sidebar-icon" src={comments} alt="Reports" />}>
                    <Link to="/reports">Відгуки</Link>
                </Menu.Item>
            </Menu>
        </div>
    );
}

export default Sidebar;
