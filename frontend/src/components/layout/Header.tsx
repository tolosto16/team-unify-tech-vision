import { Button, Layout, Input } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import React from 'react';
import './Header.scss';
import logo from '../../logo.svg';
import notifications from '../../assets/img/bell.svg';
import profile from '../../assets/img/Group.svg';
import { Link } from 'react-router-dom';

export default function Header() {
    return (
        <Layout.Header className="header">
            <div className="logo-container">
                <img src={logo} alt="Logo" className="logo" />
            </div>
            <div className="search-container">
                <Input
                    prefix={<SearchOutlined />}
                    placeholder="Пошук"
                    onPressEnter={(e) => console.log(e.currentTarget.value)}
                    className="search"
                />
            </div>
            <div className="actions-container">
                <Link to="/notifications">
                    <Button type="primary" className="button">
                        <img src={notifications} alt="Notifications" className="icon" />
                    </Button>
                </Link>
                <Link to="/profile">
                    <Button type="primary" className="button">
                        <img src={profile} alt="Profile" className="icon" />
                    </Button>
                </Link>
            </div>
        </Layout.Header>
    );
}
