import React, { useEffect, useState } from 'react';
import { Row, Col, Typography } from 'antd';
import { ComposableMap, Geographies, Geography } from 'react-simple-maps';
import { Radio } from 'antd';
import { PieChart, Pie, Tooltip, Cell, Legend } from 'recharts';
import './Home.scss';
import loaderData from '../../assets/bee-lounging.json';
import lottie from 'lottie-web';


export default function Home() {
    const [geoData, setGeoData] = useState(null);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        fetch(geoUrl)
            .then(res => res.json())
            .then(data => setGeoData(data));
    }, []);

    useEffect(() => {
        const lottieAnimation = lottie.loadAnimation({
            container: document.getElementById('lottie'),
            renderer: 'svg',
            loop: true,
            autoplay: true,
            animationData: loaderData,
        });

        return () => lottieAnimation.destroy();
    }, []);

    useEffect(() => {
        const timer = setTimeout(() => {
            setLoading(false);
        }, 7000);

        return () => clearTimeout(timer);
    }, []);


    const geoUrl =
        "https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/topojson-maps/ne_10m_admin_1_Ukraine_provinces.json";

    useEffect(() => {
        fetch(geoUrl)
            .then(res => res.json())
            .then(data => setGeoData(data));
    }, []);

    const COLORS = ["#E2F2FF", "#4D90FE", "#002766"];

    function randomValue(min: number, max: number) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    const [scale, setScale] = useState(0);
    const [data, setData] = useState({});

    const generateData = () => {
        const newData = {};
        const geoDataToUse: any = geoData;
        if (geoData && geoDataToUse?.features) {
            for (const d of geoDataToUse.features) {
                (newData as any)[d.id] = randomValue(1, 6000);
            }
        }
        setData(newData);
    };

    const colorScale = (value: number) => {
        if (value < 1000) return COLORS[0];
        if (value < 3000) return COLORS[1];
        return COLORS[2];
    };

    const data1 = [
        { name: 'Telegram', value: 36 },
        { name: 'Viber', value: 64 }
    ];

    const data2 = [
        { name: 'Задоволені', value: 92 },
        { name: 'Незадоволені', value: 8 }
    ];

    const COLORS1 = ['#63BFF3', '#A464C2'];
    const COLORS2 = ['#5EC05C', '#C05C5C']

    return (
        <>
            {loading ? (
                <div className="loader">
                    <div id="lottie"></div>
                </div>
            ) : (
                <Row className="home-info-section about-us-section" align="top">
                    <Col span={14}>
                        <Typography.Title level={4}>Платформа</Typography.Title>
                        <Row gutter={16}>
                            <Col span={12}>
                                <PieChart width={400} height={200}>
                                    <Pie
                                        data={data1}
                                        cx={90}
                                        cy={100}
                                        innerRadius={40}
                                        outerRadius={80}
                                        fill="#8884d8"
                                        dataKey="value"
                                        label={({ value }) => `${value}%`}
                                    >
                                        {data1.map((entry, index) => (
                                            <Cell key={`cell-${index}`} fill={COLORS1[index]} />
                                        ))}
                                    </Pie>
                                    <Tooltip />
                                    <Legend />
                                </PieChart>
                            </Col>
                            <Col span={12}>
                                <PieChart width={400} height={200}>
                                    <Pie
                                        data={data2}
                                        cx={90}
                                        cy={100}
                                        innerRadius={40}
                                        outerRadius={80}
                                        fill="#8884d8"
                                        dataKey="value"
                                        label={({ value }) => `${value}%`}
                                    >
                                        {data2.map((entry, index) => (
                                            <Cell key={`cell-${index}`} fill={COLORS2[index]} />
                                        ))}
                                    </Pie>
                                    <Tooltip />
                                    <Legend />
                                </PieChart>
                            </Col>
                        </Row>
                    </Col>
                    <div>
                        <h3>Активність за геолокацією</h3>
                        <Radio.Group onChange={e => setScale(e.target.value)}>
                            <Radio.Button value={0}>Менше 1 тис.</Radio.Button>
                            <Radio.Button value={1}>1-3 тис.</Radio.Button>
                            <Radio.Button value={2}>Більше 3 тис.</Radio.Button>
                        </Radio.Group>

                        <ComposableMap projection="geoAzimuthalEqualArea" projectionConfig={{ rotate: [-32.0, -49.0, 0], scale: 4500 }}>
                            <Geographies geography={geoUrl}>
                                {({ geographies }: any) =>
                                    geographies.map((geo: any) => {
                                        const cur = (data as any)[geo.id];
                                        return (
                                            <Geography
                                                key={geo.rsmKey}
                                                geography={geo}
                                                fill={colorScale(cur)}
                                            />
                                        );
                                    })
                                }
                            </Geographies>
                        </ComposableMap>
                    </div>
                </Row>
            )}
        </>
    );
}
