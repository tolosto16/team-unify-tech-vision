import { ConfigProvider } from 'antd';
import React from 'react';

export default function AntdConfigProvider({ children }: React.PropsWithChildren) {
    return (
        <ConfigProvider
            theme={{
                token: {
                    colorPrimary: '#5CB0C0',
                    borderRadius: 32,
                    colorBgLayout: '#FBFBFB',
                    colorLink: '#000000',
                },
            }}
        >
            {children}
        </ConfigProvider>
    )
}
